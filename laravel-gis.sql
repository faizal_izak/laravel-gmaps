-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Okt 2020 pada 09.46
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-gis`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategories`
--

CREATE TABLE `kategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategories`
--

INSERT INTO `kategories` (`id`, `name`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'Traffic Light', 'traffic_light.png', '2017-12-24 04:39:42', '2020-08-08 08:35:49'),
(2, 'Warning Light', 'warning_light.png', '2017-12-24 04:40:18', '2020-08-08 08:36:12'),
(3, 'PCTL', 'pctl.png', '2017-12-10 05:54:46', '2020-08-08 08:58:30'),
(4, 'Rambu Konvesional STD', 'Rambu_std.png', '2017-12-24 04:40:33', '2020-08-20 01:02:31'),
(5, 'RPPJ', 'penunjuk_arah.png', '2017-12-24 04:40:48', '2020-08-20 01:15:08'),
(6, 'Rambu Elektronik', 'rambu_elektronik.png', '2017-12-10 05:54:40', '2020-08-20 01:24:44'),
(7, 'Marka Jalan', 'marka_jalan.png', '2017-12-10 06:49:38', '2020-08-20 01:32:00'),
(8, 'Alat Pembatas kecepatan', 'pembatas_kecepatan.png', '2020-08-20 01:35:21', '2020-08-20 02:07:57'),
(9, 'Pembatas Tinggi', 'pembatas_tinggi.png', '2020-08-20 02:14:16', '2020-08-20 02:14:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `maps`
--

CREATE TABLE `maps` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategory_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `maps`
--

INSERT INTO `maps` (`id`, `kategory_id`, `title`, `description`, `no_telp`, `lat`, `long`, `foto`, `upload`, `created_at`, `updated_at`) VALUES
(55, 3, 'Kandat pusat', 'aman terkendali', NULL, '-7.9316552', '112.0226993', NULL, NULL, '2020-08-26 18:51:33', '2020-10-13 09:14:13'),
(56, 1, 'Perempatan Desa Paron', 'Kondisi traffic Light normal', NULL, '-7.814092667778048', '112.05637125268952', NULL, NULL, '2020-10-11 23:21:16', '2020-10-11 23:22:01'),
(57, 5, 'Dekat masjid Baiturahman', 'Rambu dalam kondisi baik', NULL, '-7.8179511783442726', '112.06283699999999', NULL, NULL, '2020-10-11 23:36:03', '2020-10-11 23:36:03'),
(58, 2, 'jl. Pare raya', 'aman lur', NULL, '-7.76778364103231', '112.19134948795158', NULL, NULL, '2020-10-15 00:50:09', '2020-10-15 00:50:09'),
(59, 4, 'jl. Wates raya', 'Agak sedikit hancur', NULL, '-7.921088820349928', '112.1249176701402', NULL, 'jalanwates.jpg', '2020-10-15 00:52:29', '2020-10-15 00:52:29'),
(60, 6, 'Jl. SLG', 'Sedikit kropos', NULL, '-7.818446199064262', '112.06262371175517', NULL, NULL, '2020-10-15 00:53:11', '2020-10-15 00:53:11'),
(61, 9, 'jl. Raya kandat', 'Semplah satu', NULL, '-7.928636486785093', '112.03202489636426', NULL, NULL, '2020-10-15 00:54:04', '2020-10-15 00:54:04'),
(62, 7, 'Jl. Raya Mojo', 'Aman', NULL, '-7.870707575459658', '111.97647974228465', NULL, NULL, '2020-10-15 00:54:39', '2020-10-15 00:54:39'),
(73, 5, 'ugjh', NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-15 07:02:43', '2020-10-15 07:02:43'),
(74, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-15 07:05:40', '2020-10-15 07:05:40'),
(75, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-15 07:07:03', '2020-10-15 07:07:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_05_172327_create_aplikasis_table', 1),
(4, '2017_12_05_180506_create_videos_table', 2),
(5, '2017_12_10_181534_create_maps_table', 3),
(6, '2017_12_10_195308_create_kategories_table', 3),
(7, '2017_12_27_071426_create_settings_table', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `setting_name`, `setting_value`, `created_at`, `updated_at`) VALUES
(1, 'app_name', 'Brillian Marker', '2017-12-26 18:34:03', '2020-10-11 23:11:09'),
(2, 'set_zoom', '12', '2017-12-26 18:34:11', '2020-10-11 23:11:09'),
(3, 'latitude_centre', '-7.815939', '2017-12-26 18:34:34', '2020-10-11 23:11:09'),
(4, 'longitude_centre', '112.062837', '2017-12-26 18:34:43', '2020-10-11 23:11:09'),
(5, 'gmaps_api_key', 'AIzaSyDp7xs-1CCNN6ySffyNaqGEcJDhsFr-T7g', '2017-12-26 18:34:43', '2020-10-11 23:11:09'),
(6, 'app_name', 'BS Maps', NULL, NULL),
(7, 'gmaps_api_key', 'fill use your api key', NULL, NULL),
(8, 'latitude_centre', '-6.987237', NULL, NULL),
(9, 'longitude_centre', '109.103146', NULL, NULL),
(10, 'set_zoom', '13', NULL, NULL),
(11, 'app_name', 'BS Maps', NULL, NULL),
(12, 'gmaps_api_key', 'fill use your api key', NULL, NULL),
(13, 'latitude_centre', '-6.987237', NULL, NULL),
(14, 'longitude_centre', '109.103146', NULL, NULL),
(15, 'set_zoom', '13', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `level`, `created_at`, `updated_at`, `role`) VALUES
(2, 'User', 'user', 'aziz@gmail.com', '$2y$10$6GT4Q0geLifb.4kQRO.iuufmS0O1jM2hT3OKsTVO5jyL1yDdu8G1O', 'h0KOXgBn1MxJVPffZqcCWbyo9TjpB1fqBtD53TvimGOPaKXYXdbBtR2QPQRp', 'user', NULL, NULL, NULL),
(3, 'Admin', 'admin', 'admin@gmail.com', '$2y$10$bFtRzoO0BkFhIdLY16YYR.OCTJKfHGLY.KOQY7MYMLoo4Ttou.u9u', 'Ej2VGjSqKWcTfalZ3d9pflMQuGrghy8xi1nLeI9DV9wsFKjH39aVnjXq80ka', 'admin', NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kategories`
--
ALTER TABLE `kategories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kategories`
--
ALTER TABLE `kategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
