@extends('layouts.adminlte')
@section('header')
@lang('home.dashboard')
@endsection
@section('header_description')
{{$document['header_description']}}
@endsection
@section('content')
<style type="text/css">
    .no-padding {
     padding: 0px; 
 }
</style>
   <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{totalMap()}}</h3>

                <p>Total Map</p>
              </div>
              <div class="icon">
                <i class="fa fa-map"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{totalKategory()}}</h3>

                <p>Total Kategory</p>
              </div>
              <div class="icon">
                <i class="fa fa-gear"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        
        
        </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Grafik Maps</h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart" width="600" height="200"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">@lang('home.maps')</div>

    <div class="panel-body no-padding">
       <div id="map"></div>
   </div>
</div>
@endsection

@push('js')
<script>
    var map = new GMaps({
      el: '#map',
      zoom: {{ $set_zoom }},
      lat: '{{ $latitude_centre }}',
      lng: '{{ $longitude_centre }}'
  });

    @foreach($data as $d)
    map.addMarker({
        lat: '{{$d->lat}}',
        lng: '{{$d->long}}',
        title: '{{$d->title}} #',
        icon: 'img/{{$d->Kategori->icon}}',
        infoWindow: {
            content : '<h3>{{$d->title}}</h3><p>{{$d->description}}</p><p>{{$d->no_telp}}</p>'
        }
    });
    @endforeach
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script type="text/javascript">
    var ctx = document.getElementById("myChart").getContext('2d');
    @if($grafik!=null)
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!!$grafik['labels']!!},
            datasets: [{
                label: '# of Kategori',
                data: {{$grafik['data']}},
                backgroundColor: {!!$grafik['backgroundColor']!!},
                borderColor: {!!$grafik['backgroundColor']!!},
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    @endif
</script>
@endpush