@extends('user.adminlte')
@section('header')
@lang('home.dashboard')
@endsection
@section('header_description')
{{$document['header_description']}}
@endsection
@section('content')
<style type="text/css">
    .no-padding {
     padding: 0px; 
 }
</style>
   <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{totalMap()}}</h3>

                <p>Total Map</p>
              </div>
              <div class="icon">
                <i class="fa fa-map"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{totalKategory()}}</h3>

                <p>Total Kategory</p>
              </div>
              <div class="icon">
                <i class="fa fa-gear"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        
        
        </div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Grafik Maps</h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart" width="800" height="200"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">@lang('home.maps')</div>

<style type="text/css">
  .no-padding {
    padding: 0px; 
  }
  .container, .container-fluid {
    padding-left: 0px;
    padding-right: 0px;
  }
  .filter-gmaps{
    position: absolute;
    z-index: 999;
    width: 600px;
    height: 400px;
    margin: 69px;
    /*width: 100px;*/
  }
  .summary-gmaps{
    position: absolute;
    z-index: 999;
    margin-top: 121px;
    margin-left: 10px;
    width: 199px;
  }
  .form-inline{
    display: flex;
  }
  .panel {
    margin-bottom: 22px;
    background-color: #ffffff3d;
    border: 1px solid transparent;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
  }
</style>
<div class="filter-gmaps">
  <form action="" method="GET" role="form" class="form-inline">
    {!! Form::select('kategori_id',$kat,null,['class'=>'form-control','placeholder'=>'Choose Category']) !!}
    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
  </form>
</div>
<div class="summary-gmaps">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Data</h3>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Data</th>
          <th>Jum</th>
        </tr>
      </thead>
      <tbody>
        <?php $jml=0; ?>
        @foreach($kategoryCount as $cc)

        <tr>
          <td>{{$cc->name}}</td>
          <td>{{$cc->jml}}</td>
        </tr>
        <?php $jml+=$cc->jml; ?>
        @endforeach
        <tr>
          <td><strong>Total</strong></td>
          <td><strong>{{$jml}}</strong></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
    <div class="panel-body no-padding">
       <div id="map"></div>
   </div>
</div>


@push('js')
<script src="{{ asset('js/app.js') }}"></script>
    <script>
        var map = new GMaps({
          el: '#map',
          zoom: {{$set_zoom}},
          lat: {{ $latitude_centre }},
          lng: {{ $longitude_centre }}
      });

        @foreach($data as $d)
        map.addMarker({
            lat: '{{$d->lat}}',
            lng: '{{$d->long}}',
            title: '{{$d->title}} #',
            icon: 'img/{{$d->Kategori->icon}}',
            infoWindow: {
                content : '<h3>{{$d->title}}</h3><p>{{$d->description}}</p><p>{{$d->no_telp}}</p>'
            }
        });
        @endforeach
    </script>

    @stack('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script type="text/javascript">
    var ctx = document.getElementById("myChart").getContext('2d');
    @if($grafik!=null)
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!!$grafik['labels']!!},
            datasets: [{
                label: '# of Kategori',
                data: {{$grafik['data']}},
                backgroundColor: {!!$grafik['backgroundColor']!!},
                borderColor: {!!$grafik['backgroundColor']!!},
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    @endif
</script>
@endpush
@endsection