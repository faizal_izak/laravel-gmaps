<html>
<head>
	<title>MAP PDF </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="panel panel-default">
     <table border="0" align="center">
        <tr>
        <td width="50%"><img src="img/logo_dishub.jpg" width="70" height="70"></td>
        <td><center>
          <font size="5"><b>&emsp; DINAS PERHUBUNGAN KABUPATEN KEDIRI</b></font>
          <font>              </font><br>
          <font size="2"><i>Alamat: Kawasan Monumen SLG Kab Kediri Telp./Fax ( 0354 ) 545400</i></font>
        </center></td>
        <hr>
      </tr>
      </table>
		<div class="panel-heading">
			<h3 align = "center">DAFTAR MAP</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
        <thead>
          <tr>
            <th> Kategori </th>
            <th> Lokasi </th>
            <th> Deskripsi </th>
          </tr>
        </thead>
        <tbody>
          @foreach ($maps as $data)

          <tr>
            <td> {{ $data -> Kategori->name}}</td>
            <td> {{ $data -> title}}</td>
            <td> {{ $data -> description}}</td>
          </tr>
          @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</body>
	</html>