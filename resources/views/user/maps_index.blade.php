@extends('user.adminlte')

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">Maps</div>
                <div class="panel-body">

                    <a href="maps_pdf" class="btn btn-primary" title="Cetak PDF"><span class="glyphicon glyphicon-print" aria-hidden="true"/></a>
                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <!-- <th>ID</th> -->
                                    <th> Kategori </th><th> Lokasi </th><th> Deskripsi </th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($maps as $item)
                                <tr>
                                    <!-- <td>{{ $item->id }}</td> -->
                                    <td>{{ $item->Kategori->name }}</td><td>{{ $item->title }}</td><td>{{ $item->description }}</td>
                                    <td>
                                        <a href="{{ route('detailmaps',$item->id) }}" class="btn btn-warning" title="Detail"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/> Detail</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $maps->render() !!} </div>
                        </div>

                    </div>
                </div>
    @endsection