<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','HomeController@welcome');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/home', 'HomeController@homeUser')->name('user/home');

// Route::resource('aplikasi', 'AplikasiController');
// Route::resource('video', 'VideoController');
Route::get('/user/maps', 'MapsController@indexUser')->name('user/maps');
Route::get('user/maps_pdf', 'MapsController@cetakPdf')->name('maps_pdf');
Route::get('detailmaps/{id}', 'MapsController@editmaps')->name('detailmaps');
Route::resource('maps', 'MapsController');
Route::resource('kategory', 'KategoryController');
Route::resource('setting', 'SettingController');